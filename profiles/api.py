from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from tastypie.http import HttpUnauthorized, HttpForbidden
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie import fields
from tastypie.authorization import DjangoAuthorization
from tastypie.authentication import BasicAuthentication
from tastypie.resources import ModelResource
from coursemanager.api import (CourseResource, LessonMaterialResource,
                               QuestionResource)
from .models import (UserProfile, LoginRecord, UserLessonPlan, UserLesson,
                     LessonRecord, SessionInformation, VideoPauses,
                     QuestionData)


class UserResource(ModelResource):

    class Meta:
        queryset = User.objects.all()
        fields = ['first_name', 'last_name', 'email']
        allowed_methods = ['get', 'post']
        resource_name = 'user'

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r'^(?P<resource_name>%s)/logout%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
        ]

    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request,
                                request.body,
                                format=request.META.get('CONTENT_TYPE',
                                                        'application/json'))

        username = data.get('username', '')
        password = data.get('password', '')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return self.create_response(request, {
                    'success': True
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'reason': 'disabled',
                    }, HttpForbidden)
        else:
            return self.create_response(request, {
                'success': False,
                'reason': 'incorrect',
                }, HttpUnauthorized)

    def logout(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if request.user and request.user.is_authenticated():
            logout(request)
            return self.create_response(request, {'success': True})
        else:
            return self.create_response(request, {'success': False},
                                        HttpUnauthorized)


class UserProfileResource(ModelResource):

    user = fields.OneToOneField(UserResource, 'user')
    course = fields.ManyToManyField(CourseResource, 'course')

    class Meta:
        queryset = UserProfile.objects.all()
        resource_name = 'userprof'
        authorization = DjangoAuthorization()
        authentication = BasicAuthentication()


class LoginRecordResource(ModelResource):

    profile = fields.ForeignKey(UserProfileResource, 'profile')

    class Meta:
        queryset = LoginRecord.objects.all()
        resource_name = 'loginrec'
        authorization = DjangoAuthorization()
        authentication = BasicAuthentication()


class UserLessonPlanResource(ModelResource):

    profile = fields.ForeignKey(UserProfileResource, 'profile')

    course = fields.ForeignKey(CourseResource, 'course')

    class Meta:
        queryset = UserLessonPlan.objects.all()
        resource_name = 'userplan'
        authorization = DjangoAuthorization()
        authentication = BasicAuthentication()


class UserLessonResource(ModelResource):

    lesson_plan = fields.ForeignKey(UserLessonPlanResource, 'lesson_plan')

    lesson_content = fields.ForeignKey(LessonMaterialResource,
                                       'lesson_content')

    class Meta:
        queryset = UserLesson.objects.all()
        resource_name = 'userlesson'
        authorization = DjangoAuthorization()
        authentication = BasicAuthentication()


class LessonRecordResource(ModelResource):

    profile = fields.ForeignKey(UserProfileResource, 'profile')
    material = fields.ForeignKey(LessonMaterialResource, 'material')

    class Meta:
        queryset = LessonRecord.objects.all()
        resource_name = 'lessonrec'
        authorization = DjangoAuthorization()
        authentication = BasicAuthentication()


class SessionInformationResource(ModelResource):

    record = fields.ForeignKey(LessonRecordResource, 'record')

    class Meta:
        queryset = SessionInformation.objects.all()
        resource_name = 'sessioninfo'
        authorization = DjangoAuthorization()
        authentication = BasicAuthentication()


class VideoPausesResource(ModelResource):

    session = fields.ForeignKey(SessionInformationResource, 'session')

    class Meta:
        queryset = VideoPauses.objects.all()
        resource_name = 'vidpause'
        authorization = DjangoAuthorization()
        authentication = BasicAuthentication()


class QuestionDataResource(ModelResource):

    session = fields.ForeignKey(SessionInformationResource, 'session')
    question = fields.ForeignKey(QuestionResource, 'question')

    class Meta:
        queryset = QuestionData.objects.all()
        resource_name = 'qdata'
        authorization = DjangoAuthorization()
        authentication = BasicAuthentication()
