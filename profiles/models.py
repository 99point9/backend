from django.db import models
from django.contrib.auth.models import User
from coursemanager.models import Course, LessonMaterial, Question


class UserProfile(models.Model):
    user = models.OneToOneField(User, null=False, primary_key=True,
                                verbose_name='Member profile')
    course = models.ManyToManyField(Course)

    def __str__(self):
        return self.user.username


class LoginRecord(models.Model):

    profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    loginTime = models.DateTimeField()
    ip = models.GenericIPAddressField()

    def __str__(self):
        return (self.profile.user.username + ' Login At '
                + self.loginTime.isoformat())


class UserLessonPlan(models.Model):

    profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)

    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    lesson_name = models.CharField('Lesson Plan Name', max_length=200)
    description = models.CharField('Description', max_length=800)

    def __str__(self):
        return (self.lesson_name + '-' + self.course.course_name + ' for '
                + self.profile.user.username)


class UserLesson(models.Model):

    lesson_plan = models.ForeignKey(UserLessonPlan,
                                    on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField('Order in User Lesson Plan',
                                             default=0)
    lesson_content = models.ForeignKey(LessonMaterial,
                                       on_delete=models.CASCADE)

    class Meta:
        ordering = ('order',)

    def __str__(self):
        return self.lesson_plan.lesson_name + '-' + str(self.order)


class LessonRecord(models.Model):

    profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    material = models.ForeignKey(LessonMaterial, on_delete=models.CASCADE)

    completedbool = models.BooleanField('Lesson Completed?', default=False)

    def __str__(self):
        return (self.profile.user.username + ' Progress On '
                + self.material.__str__())


class SessionInformation(models.Model):

    record = models.ForeignKey(LessonRecord, on_delete=models.CASCADE)
    accessStart = models.DateTimeField('Time lesson was accessed')
    accessEnd = models.DateTimeField('Time this access ended')

    def __str__(self):
        return (self.record.__str__() + ' Access At '
                + self.accessStart.isoformat())


class VideoPauses(models.Model):

    session = models.ForeignKey(SessionInformation, on_delete=models.CASCADE)

    startPause = models.DateTimeField('Start of Pause')
    endPause = models.DateTimeField('End of Pause')
    pausetime = models.TimeField('Timestamp of Video at Pause')

    def __str__(self):
        return (self.session.__str__() + ' Pause At '
                + self.startPause.isoformat())


class QuestionData(models.Model):

    session = models.ForeignKey(SessionInformation, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    mistakes = models.SmallIntegerField('Mistakes Made Before Correct Answer',
                                        default=0)

    def __str__(self):
        return (self.session.__str__() + ' data for question '
                + str(self.question.order))


class AuthorizedMail(models.Model):

    email = models.CharField('Authorized E-mail', max_length=200)

    def __str__(self):
        return self.email
