from django.contrib import admin
import nested_admin
from .models import *


class QuestionDataInline(nested_admin.NestedStackedInline):
    model = QuestionData
    extra = 0


class VideoPausesInline(nested_admin.NestedStackedInline):
    model = VideoPauses
    extra = 0


class SessionInformationInline(nested_admin.NestedStackedInline):
    model = SessionInformation
    inlines = [VideoPausesInline, QuestionDataInline]
    extra = 0


class LessonRecordInline(nested_admin.NestedStackedInline):
    model = LessonRecord
    inlines = [SessionInformationInline]
    extra = 0


class UserLessonInline(nested_admin.NestedStackedInline):
    model = UserLesson
    extra = 0


class UserLessonPlanInline(nested_admin.NestedStackedInline):
    model = UserLessonPlan
    inlines = [UserLessonInline]
    extra = 0


class LoginRecordInline(nested_admin.NestedStackedInline):
    model = LoginRecord
    extra = 0


class UserProfileAdmin(nested_admin.NestedModelAdmin):
    inlines = [LoginRecordInline, UserLessonPlanInline, LessonRecordInline]


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(AuthorizedMail)
