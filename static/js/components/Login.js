import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import '../../styles/Login.css';
import { BrowserRouter as Router,
          Route, Switch, Redirect, Link } from 'react-router-dom';
import App from './App'
import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';

class LoginPage extends Component {

  constructor(props) {
    super(props);

    this.state={
      username:'',
      password:'',
      isLoggedIn: false
    };

  }

  userLogin() {
    this.setState({
      isLoggedIn: true
    });
  }

  userLogout() {
    this.setState({
      isLoggedIn: false
    });
  }

  handleClick(event){
   const apiBaseUrl = "http://138.197.154.10/api/profiles/";
   var self = this;
   var payload={
     "username":this.state.username,
     "password":this.state.password
    };

    axios.post(apiBaseUrl+'user/login/', payload)
    .then(function (response) {
    console.log(response);

    if(response.status == 200){
    console.log("Login successful");
      self.userLogin();
    }
    else if(response.status == 204){
    console.log("Username password do not match");
    alert("username password do not match");
    }
    else{
    console.log("Username does not exists");
    alert("Username does not exist");
    }
    })
    .catch(function (error) {
    console.log(error);
    });
  }

  render() {

    const logcheck = this.state.isLoggedIn;

    if (logcheck) {
      return <App username={this.state.username} logout={this.userLogout()}/>;
    }

    return(
      <div className="login-container">
        <Paper zDepth={2} style={style2}>
          <div>
            <img className="logo" src="http://138.197.154.10/media/logo-red.png" alt="" />
          </div><br />
         <TextField
           hintText="Enter your Username"
           floatingLabelText="Username"
           onChange = {(event,newValue) => this.setState({username:newValue})}
           />
         <br/>
           <TextField
             type="password"
             hintText="Enter your Password"
             floatingLabelText="Password"
             onChange = {(event,newValue) => this.setState({password:newValue})}
             />
           <br/>
           <Link to='/app/select'><RaisedButton label="Login" primary={true} style={style} /></Link>
           <Link to='/register'><RaisedButton label="Register" primary={true} style={style} /></Link>
       </Paper>
     </div>
   );
  }
}

class Registration extends Component {

  render() {

    return(
      <div className="login-container">
        <Paper zDepth={2} style={style2}>
          <div>
            <img className="logo" src="http://138.197.154.10/media/logo-red.png" alt="" />
          </div><br />
          <TextField
            hintText="First Name"
            floatingLabelText="First Name"
            />
          <br/>
          <TextField
            hintText="Last Name"
            floatingLabelText="Last Name"
            />
          <br/>
          <TextField
            hintText="Phone Number"
            floatingLabelText="Mobile Number"
            />
          <br/>
          <TextField
            hintText="Enter your E-mail"
            floatingLabelText="E-mail"
            />
          <br/>
         <TextField
           hintText="Enter your desired username"
           floatingLabelText="Desired Username"
           />
         <br/>
         <TextField
           type="password"
           hintText="Enter your desired password"
           floatingLabelText="Desired Password"
           />
          <br/>
          <TextField
            type="password"
            hintText="Confirm your password"
            floatingLabelText="Confirm Password"
            />
           <br/>
          <RaisedButton label="Submit" primary={true} style={style} /><br/>
       </Paper>
      </div>
    );
  }

}

class Login extends Component {

  render() {

    return(
       <Switch>
         <Route className='center-box' path='/login' component={LoginPage}/>
         <Route className='center-box' path='/register' component={Registration}/>
         <Route path='/app/select' component={App}/>
         <Redirect to="/login" />
       </Switch>
     )
  }
}

const style = {
 margin: 15,
};

const style2 = {
  margin: 40,
  padding: 100,
  textAlign: 'center',
  display: 'inline-block',
};


export default Login;
