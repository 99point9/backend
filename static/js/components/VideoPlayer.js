
import React, { PropTypes, Component } from 'react';
import { BigPlayButton, Player, ControlBar, PlayToggle, ReplayControl,
  ForwardControl, CurrentTimeDisplay, Shortcut,
  TimeDivider, PlaybackRateMenuButton, VolumeMenuButton
} from 'video-react';
import SpeechRecognition from 'react-speech-recognition';
import '../../styles/App.css';
import '../../styles/VideoPlayer.css';
import '../../../node_modules/video-react/dist/video-react.css';
import axios from 'axios';
import Dialog from 'material-ui/Dialog';

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

const options = {
  autoStart: false
}

var shouldHide = false;

var finalTranscriptRecord = '';

const video = "http://138.197.154.10:3000/media/LSAT/test1/Translation.mp4"

const questions = ["http://138.197.154.10:3000/media/LSAT/test1/Q1.mp4", "http://138.197.154.10:3000/media/LSAT/test1/Q2.mp4"]

const answers = ["http://138.197.154.10:3000/media/LSAT/test1/A1.mp4", "http://138.197.154.10:3000/media/LSAT/test1/A2.mp4"]

class VideoPlayer extends Component {

  constructor(props) {
    super(props);
    const { transcript,
            finalTranscript,
            interimTranscript,
            resetTranscript,
            browserSupportsSpeechRecognition,
            startListening,
            stopListening,
            abortListening,
            listening,
            recognition,
          } = this.props;

    this.checkState = this.checkState.bind(this);
    this.checkAudioState = this.checkAudioState.bind(this);
    this.playv = this.playv.bind(this);

    this.state = {
      videocount: 1,
      questioncount: 1,
      open: false
    };

  }

  playv() {
    this.refs.player.play();
  }

  checkState() {
    const { player } = this.refs.player.getState();
    var x = player.currentTime;
    if (x > 5 && x < 6 && q1pass === false) {
      console.log('this worked');
      shouldHide = true;
      this.refs.player.pause();

      this.refs.q1.play();
    }
    if (x > 10 && x < 11 && q2pass === false) {
      console.log('this worked');
      shouldHide = true;
      this.refs.player.pause();
      this.refs.q2.play();
    }
  }

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.listening === true) {
      if (this.props.finalTranscript != '') {
        finalTranscriptRecord = this.props.finalTranscript;
        this.props.resetTranscript();
      }
      const { player } = this.refs.player.getState();
      var x = player.currentTime;
      if (x > 5 && x < 7) {
        if (this.props.finalTranscript === 'sedimentation') {
          q1pass = true;
          this.props.stopListening();
          this.refs.a1.play();
          shouldHide = false;
        }
      }
      if (x > 10 && x < 12) {
        if (this.props.finalTranscript === 'starts it') {
          q2pass = true;
          this.props.stopListening();
          this.refs.a2.play();
          shouldHide = false;
        }
      }
    }

  }

  checkAudioState() {
    this.props.startListening();
  }

  render() {
    const { transcript,
            finalTranscript,
            interimTranscript,
            resetTranscript,
            browserSupportsSpeechRecognition,
            startListening,
            stopListening,
            abortListening,
            listening,
            recognition,
          } = this.props;

          const actions = [
            <TextField
              hintText="Type your answer here if voice recognition isn't working."
              floatingLabelText="Answer Field"
              />
            <FlatButton
              label="Replay Audio"
              primary={true}
              onClick={this.handleClose}
            />,
            <FlatButton
              label="Submit"
              primary={true}
              disabled={true}
              onClick={this.handleClose}
            />,
          ];

    if (!browserSupportsSpeechRecognition) {
      return null
    }

    var partial;

    if (shouldHide === false) {
      partial = <PlayToggle />
    }

    return (
      <div className="contentboxwrapper">

        <div className="contentboxtop" id="boxsizevideotop">
          <h3>Placeholder Video Title</h3>
        </div>

        <div className="contentbox" id="boxsizevideo">
          <div id="videobox">
            <Player ref="player" onTimeUpdate={this.checkState}>
              <source src={video} />
              <Shortcut disabled />
              <ControlBar autoHide={false} disableDefaultControls={true}>
                {partial}
                <ReplayControl seconds={10} order={1.1} />
                <CurrentTimeDisplay order={4.1} />
                <TimeDivider order={4.2} />
                <PlaybackRateMenuButton disabled />
                <VolumeMenuButton enabled />
              </ControlBar>
            </Player>
          </div>
          <div id="videocontrols">
            <br/>
            <br/>
            <br/>
            <button onClick={resetTranscript}>Reset</button>
            <br/><p>Transcription Test:</p>
            <span>{finalTranscriptRecord}</span>
          </div>

          <audio ref='q1' src={questions[0]} onEnded={this.checkAudioState}/>
          <audio ref='q2' src={questions[1]} onEnded={this.checkAudioState}/>
          <audio ref='a1' src={answers[0]} onEnded={this.playv}/>
          <audio ref='a2' src={answers[1]} onEnded={this.playv}/>
        </div>

          <div>
            <RaisedButton label="Question Popup Test" onClick={this.handleOpen} />
            <Dialog
              title="Question 1"
              actions={actions}
              modal={false}
              open={this.state.open}
            >
              Only actions can close this dialog.
            </Dialog>
          </div>

      </div>
    );
  }
}

export default SpeechRecognition(options)(VideoPlayer);
