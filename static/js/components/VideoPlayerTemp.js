import React, { PropTypes, Component } from 'react';
import { BigPlayButton, Player, ControlBar, PlayToggle, ReplayControl,
  ForwardControl, CurrentTimeDisplay, Shortcut,
  TimeDivider, PlaybackRateMenuButton, VolumeMenuButton
} from 'video-react';
import SpeechRecognition from 'react-speech-recognition';
import '../../styles/App.css';
import '../../styles/VideoPlayer.css';
import '../../../node_modules/video-react/dist/video-react.css';
import axios from 'axios';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton'
import VideoMenu from 'material-ui/svg-icons/av/video-library';
import GoButton from 'material-ui/svg-icons/action/launch';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import Subheader from 'material-ui/Subheader';
import Drawer from 'material-ui/Drawer';
import {GridList, GridTile} from 'material-ui/GridList';
import {Toolbar, ToolbarGroup, ToolbarTitle} from 'material-ui/Toolbar';

axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';

const options = {
  autoStart: false
}

var finalTranscriptRecord = '';

const course = "MCAT";

const baseURL = "http://138.197.154.10:3000/media/";

var vidlist = ["LSAT/Argument%20BASICS/Argument_Basics_-_1.mp4",
                "LSAT/Logic%20Rules%3A%20Symbolizing%20Names/Symbols_2.mp4",
                "LSAT/NEGATION/Negation.mp4",
                "LSAT/DOUBLE NEGATION/Double_Negation.mp4",
                "LSAT/LSAT: Logic rules - DISJUNCTIONS/Disjunction.mp4",
                "LSAT/Logic%20Rules%3A%20Conditionals/conditionals.mp4",
                "LSAT/Logic%20Rules%3A%20Biconditional/Biconditionals.mp4",
                "LSAT/LSAT - QT: Conclusions/QT_conclusion_question.mp4",
                "LSAT/LSAT - QT: Assumptions/QT_Assumption_question.mp4",
                "LSAT/LSAT - QT: Strengthen/QT_Strengthen_question.mp4",
                "MCAT/demo/Translation.mp4"];

vidlist = vidlist.map(function(e) {return baseURL + e});

var vidnamelist = ["Argument Basics",
                      "Logic: Symbolizing Names",
                      "Logic: Negation",
                      "Logic: Double Negation",
                      "Logic: Disjunctions",
                      "Logic: Conditionals",
                      "Logic: Biconditionals",
                      "Question Types: Conclusions",
                      "Question Types: Assumptions",
                      "Question Types: Strengthen",
                      "Translation"];

var vidcovlist = ["LSAT/Argument BASICS/1.jpg",
                    "LSAT/Logic Rules: Symbolizing Names/2.jpg",
                    "LSAT/NEGATION/3.jpg",
                    "LSAT/DOUBLE NEGATION/4.jpg",
                    "LSAT/LSAT: Logic rules - DISJUNCTIONS/5.jpg",
                    "LSAT/Logic Rules: Conditionals/6.jpg",
                    "LSAT/Logic Rules: Biconditional/7.jpg",
                    "LSAT/LSAT - QT: Conclusions/QT_conclusion_question_First_Frame.png",
                    "LSAT/LSAT - QT: Assumptions/QT_Assumption_question_First_Frame.png",
                    "LSAT/LSAT - QT: Strengthen/QT_Strengthen_question_First_Frame.png",
                    "MCAT/demo/MCAT_official_logo.jpg"];

vidcovlist = vidcovlist.map(function(e) {return baseURL + e});

var qlist = ["MCAT/demo/Q1.m4a",
             "MCAT/demo/Q2.m4a",
             "MCAT/demo/Q3.m4a",
             "MCAT/demo/Q4.m4a",
             "MCAT/demo/Q5.m4a",
            ];

var alist = ["MCAT/demo/A1.m4a",
             "MCAT/demo/A2.m4a",
             "MCAT/demo/A3.m4a",
             "MCAT/demo/A4.m4a",
             "MCAT/demo/A5.m4a",
            ];

var qtext = ["What do Svedberg units measure, by the way?",
             "What’s an initiation factor, do you think? It probably does what to translation?",
             "What is the cap made of?",
             "And when is it added onto the mRNA? At the end of which process?",
             "We took DNA, and converted it into?"];

var atext = ["sedimentation",
             "initiates",
             "guanine",
             "transcription",
             "RNA"];

var qsecs = [24, 32, 41, 65, 131];

var qpass = [false, false, false, false, false];

var attempts = [];

const style = {
 margin: 15,
 width: 1280,
 height: 720,
};

const style2 = {
  margin: 40,
  padding: 100,
  textAlign: 'center',
  display: 'inline-block',
};

qlist = qlist.map(function(e) {return baseURL + e});
alist = alist.map(function(e) {return baseURL + e});

class VideoPlayer extends Component {

  constructor(props, context) {
    super(props, context);

    const { transcript,
            finalTranscript,
            interimTranscript,
            resetTranscript,
            browserSupportsSpeechRecognition,
            startListening,
            stopListening,
            abortListening,
            listening,
            recognition,
          } = this.props;

    this.checkState = this.checkState.bind(this);
    this.checkAudioState = this.checkAudioState.bind(this);
    this.playv = this.playv.bind(this);

    this.state = {
      vidcount: 0,
      vidtotal: 11,
      open: false,
      dopen: false,
      source: vidlist[0],
      qcount: 0
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.changeSource = this.changeSource.bind(this);

  }

  playv() {
    this.refs.player.play();
  }

  handleDToggle = () => this.setState({dopen: !this.state.dopen});

  checkState() {
    const { player } = this.refs.player.getState();
    var x = player.currentTime;
    console.log('checking state at x = ' + x);
    if (x > qsecs[this.state.qcount]
        && x < (qsecs[this.state.qcount]+2)
        && qpass[this.state.qcount] === false) {
      console.log('this worked');
      this.refs.player.pause();
      this.handleOpen();
      if (this.state.qcount == 0) {
          this.refs.q1.play();
        } else if (this.state.qcount == 1) {
            this.refs.q2.play();
          } else if (this.state.qcount == 2) {
              this.refs.q3.play();
            } else if (this.state.qcount == 3) {
                this.refs.q4.play();
              } else if (this.state.qcount == 4) {
                  this.refs.q5.play();
                }
    }
  }

  componentDidMount() {
    // subscribe state change
    this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this));
    var videon = parseInt(this.props.match.params.videon, 10);
    this.setState({
      source: vidlist[videon]
    });
    this.refs.player.load();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.listening === true) {
      if (this.props.finalTranscript != '') {
        finalTranscriptRecord = this.props.finalTranscript;
        attempts.unshift(finalTranscriptRecord);
        this.props.resetTranscript();
      }
      const { player } = this.refs.player.getState();
      var x = player.currentTime;
      if (x > qsecs[this.state.qcount] && x < (qsecs[this.state.qcount]+2)) {
        if (finalTranscriptRecord == atext[this.state.qcount]) {
          qpass[this.state.qcount] = true;
          this.props.stopListening();
          if (this.state.qcount == 0) {
              this.refs.a1.play();
            } else if (this.state.qcount == 1) {
                this.refs.a2.play();
              } else if (this.state.qcount == 2) {
                  this.refs.a3.play();
                } else if (this.state.qcount == 3) {
                    this.refs.a4.play();
                  } else if (this.state.qcount == 4) {
                      this.refs.a5.play();
                    }

          this.handleClose();

          this.setState({
            qcount: this.state.qcount + 1
          });
        }
      }
    }
  }

  handleStateChange(state, prevState) {
    // copy player state to this component's state
    this.setState({
      player: state
    });
  }

  handleToggle() {
    this.setState({open: !this.state.open});
  }

  changeSource(n) {
    return () => {
      this.setState({
        source: vidlist[n]
      });
      this.refs.player.load();
    };
  }

  handleOpen = () => {
    console.log("opened dialog");
    this.setState({open: true});
  };

  handleClose = () => {
    console.log("closed dialog");
    this.setState({open: false});
  };

  checkAudioState() {
    this.props.startListening();
  }

  render() {

    const { transcript,
            finalTranscript,
            interimTranscript,
            resetTranscript,
            browserSupportsSpeechRecognition,
            startListening,
            stopListening,
            abortListening,
            listening,
            recognition,
          } = this.props;

    var partial;

    const actions = [
    ];

    return (

        <div>
          <Paper zDepth={2} style={style}>
            <div>
              <Player ref="player" onTimeUpdate={this.checkState}>
                <source src={this.state.source} />
                <Shortcut disabled />
                <ControlBar autoHide={false} disableDefaultControls={true}>
                  <BigPlayButton position='center' />
                  <PlayToggle />
                  <ReplayControl seconds={10} order={1.1} />
                  <VolumeMenuButton order={2.1} />
                  <PlaybackRateMenuButton
                    rates={[2, 1.5, 1, 0.5]}
                    order={3.1}
                  />
                  <CurrentTimeDisplay order={4.1} />
                </ControlBar>
              </Player>
            </div>
            <RaisedButton
              label="Toggle Stats"
              onClick={this.handleDToggle}
            />
            <Drawer open={this.state.dopen} openSecondary={true}>
              <div>Question Answered? {qpass.toString()}</div>
              <div>Answer Attempts: {attempts.toString()}</div>
            </Drawer>

          </Paper>
          <div>
            <Dialog
              title={"Question " + (this.state.qcount+1)}
              actions={actions}
              modal={true}
              open={this.state.open}
            >
                <p>{qtext[this.state.qcount]}</p>
                <span>Transcription: {finalTranscriptRecord}</span>
            </Dialog>
          </div>
          <audio ref='q1' src={qlist[0]} onEnded={this.checkAudioState}/>
          <audio ref='q2' src={qlist[1]} onEnded={this.checkAudioState}/>
          <audio ref='q3' src={qlist[2]} onEnded={this.checkAudioState}/>
          <audio ref='q4' src={qlist[3]} onEnded={this.checkAudioState}/>
          <audio ref='q5' src={qlist[4]} onEnded={this.checkAudioState}/>
          <audio ref='a1' src={alist[0]} onEnded={this.playv}/>
          <audio ref='a2' src={alist[1]} onEnded={this.playv}/>
          <audio ref='a3' src={alist[2]} onEnded={this.playv}/>
          <audio ref='a4' src={alist[3]} onEnded={this.playv}/>
          <audio ref='a5' src={alist[4]} onEnded={this.playv}/>
        </div>

    );
  }
}

export default SpeechRecognition(options)(VideoPlayer);
