import React, { PropTypes, Component } from 'react';
import { BigPlayButton, Player, ControlBar, PlayToggle, ReplayControl,
  ForwardControl, CurrentTimeDisplay, Shortcut,
  TimeDivider, PlaybackRateMenuButton, VolumeMenuButton
} from 'video-react';
import SpeechRecognition from 'react-speech-recognition';
import '../../styles/Temp.css';
import '../../../node_modules/video-react/dist/video-react.css';
import Paper from 'material-ui/Paper';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import axios from 'axios';

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

const options = {
  autoStart: false
}

var shouldHide = false;

var finalTranscriptRecord = '';

var q1pass = false;
var q2pass = false;

const video = "http://138.197.154.10:3000/media/LSAT/test1/Translation.mp4"

const questions = ["http://138.197.154.10:3000/media/LSAT/TranslationTest/Q1.m4a", "http://138.197.154.10:3000/media/LSAT/TranslationTest/Q2.m4a"]

const answers = ["http://138.197.154.10:3000/media/LSAT/TranslationTest/A1.m4a", "http://138.197.154.10:3000/media/LSAT/TranslationTest/A2.m4a"]

class Temp extends Component {

  constructor(props) {
    super(props);
    const { transcript,
            finalTranscript,
            interimTranscript,
            resetTranscript,
            browserSupportsSpeechRecognition,
            startListening,
            stopListening,
            abortListening,
            listening,
            recognition,
          } = this.props;

    this.checkState = this.checkState.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.checkAudioState = this.checkAudioState.bind(this);
    this.playv = this.playv.bind(this);

    this.state = {
      videocount: 1,
      questioncount: 1,
      open: false
    };

  }

  playv() {
    this.refs.player.play();
  }

  checkState() {
    const { player } = this.refs.player.getState();
    var x = player.currentTime;
    if (x > 5 && x < 6 && q1pass === false) {
      console.log('this worked');
      shouldHide = true;
      this.refs.player.pause();
      this.refs.q1.play();
    }
    if (x > 10 && x < 11 && q2pass === false) {
      console.log('this worked');
      shouldHide = true;
      this.refs.player.pause();
      this.refs.q2.play();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.listening === true) {
      if (this.props.finalTranscript != '') {
        finalTranscriptRecord = this.props.finalTranscript;
        this.props.resetTranscript();
      }
      const { player } = this.refs.player.getState();
      var x = player.currentTime;
      if (x > 5 && x < 7) {
        if (this.props.finalTranscript === 'sedimentation') {
          q1pass = true;
          this.props.stopListening();
          this.refs.a1.play();
          shouldHide = false;
        }
      }
      if (x > 10 && x < 12) {
        if (this.props.finalTranscript === 'starts it') {
          q2pass = true;
          this.props.stopListening();
          this.refs.a2.play();
          shouldHide = false;
        }
      }
    }

  }

  checkAudioState() {
    this.props.startListening();
  }

  handleToggle() {
    this.setState({open: !this.state.open});
  }

  render() {
    const { transcript,
            finalTranscript,
            interimTranscript,
            resetTranscript,
            browserSupportsSpeechRecognition,
            startListening,
            stopListening,
            abortListening,
            listening,
            recognition,
          } = this.props;

    if (!browserSupportsSpeechRecognition) {
      return null
    }

    var partial;

    if (shouldHide === false) {
      partial = <PlayToggle />
    }

    return (
      <div>
        <Drawer open={this.state.open}>
          <MenuItem onClick={this.handleToggle}> Close </MenuItem>
          <MenuItem> 1) Translation</MenuItem>
          <MenuItem> 2) Logic</MenuItem>
        </Drawer>
        <div className="container">
          <Paper className="video-container" zDepth={2} style={style2}>
            <Player ref="player" onTimeUpdate={this.checkState}>
              <source src={video} />
              <Shortcut disabled />
              <ControlBar autoHide={false} disableDefaultControls={true}>
                {partial}
                <ReplayControl seconds={10} order={1.1} />
                <CurrentTimeDisplay order={4.1} />
                <TimeDivider order={4.2} />
                <VolumeMenuButton enabled />
                <PlaybackRateMenuButton />
              </ControlBar>
            </Player>
            <div id="video-controls">
              <RaisedButton
                label="Reset"
                onClick={resetTranscript}
              />
              <RaisedButton
                label="Menu"
                onClick={this.handleToggle}
              />
              <br/><p>Transcription Test:</p>
              <span>{finalTranscriptRecord}</span>
            </div>
            <audio ref='q1' src={questions[0]} onEnded={this.checkAudioState}/>
            <audio ref='q2' src={questions[1]} onEnded={this.checkAudioState}/>
            <audio ref='a1' src={answers[0]} onEnded={this.playv}/>
            <audio ref='a2' src={answers[1]} onEnded={this.playv}/>
          </Paper>
          </div>
        </div>

    );
  }
}

const style2 = {
  margin: 40,
  padding: 100,
  textAlign: 'center',
  display: 'inline-block',
};

export default SpeechRecognition(options)(Temp);
