import React, { Component } from 'react';
import SpeechRecognition from './VideoPlayerTemp';
import Nav from './Nav';
import '../../styles/App.css';
import {Card, CardActions, CardHeader,
        CardMedia, CardTitle, CardText} from 'material-ui/Card';
import { BrowserRouter as Router,
          Route, Switch, Redirect, Link } from 'react-router-dom';
import FlatButton from 'material-ui/FlatButton';
import axios from 'axios';
import Slider from "react-slick";
import Profile from './Profile';
import Paper from 'material-ui/Paper';

axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';

const baseURL = "http://138.197.154.10:3000/media/";

var vidnamelist = ["Argument Basics",
                      "Logic: Symbolizing Names",
                      "Logic: Negation",
                      "Logic: Double Negation",
                      "Logic: Disjunctions",
                      "Logic: Conditionals",
                      "Logic: Biconditionals",
                      "Question Types: Conclusions",
                      "Question Types: Assumptions",
                      "Question Types: Strengthen",];

var vidcovlist = ["LSAT/Argument BASICS/1.jpg",
                    "LSAT/Logic Rules: Symbolizing Names/2.jpg",
                    "LSAT/NEGATION/3.jpg",
                    "LSAT/DOUBLE NEGATION/4.jpg",
                    "LSAT/LSAT: Logic rules - DISJUNCTIONS/5.jpg",
                    "LSAT/Logic Rules: Conditionals/6.jpg",
                    "LSAT/Logic Rules: Biconditional/7.jpg",
                    "LSAT/LSAT - QT: Conclusions/QT_conclusion_question_First_Frame.png",
                    "LSAT/LSAT - QT: Assumptions/QT_Assumption_question_First_Frame.png",
                    "LSAT/LSAT - QT: Strengthen/QT_Strengthen_question_First_Frame.png",
                    "MCAT/demo/MCAT_official_logo.jpg"];

vidcovlist = vidcovlist.map(function(e) {return baseURL + e});

const style = {
 margin: 40,
};

const style2 = {
  margin: 40,
  padding: 50,
  textAlign: 'center',
};

class Select extends Component {

  render() {

    var settings = {
        dots: true,
        slidesToShow: 3,
      };

    return (
      <div>
        <Paper zDepth={2} style={style2}>
          <h1>MCAT</h1>
          <br/>
          <Slider {...settings}>
            <Link to='/app/select/10'><img className="slideimg" src={vidcovlist[10]} alt="" /></Link>
          </Slider>
        </Paper>
        <Paper zDepth={2} style={style2}>
          <h1>LSAT</h1>
          <br/>
          <Slider {...settings}>
            <Link to='/app/select/0'><img className="slideimg" src={vidcovlist[0]} alt="" /></Link>
            <Link to='/app/select/1'><img className="slideimg" src={vidcovlist[1]} alt="" /></Link>
            <Link to='/app/select/2'><img className="slideimg" src={vidcovlist[2]} alt="" /></Link>
            <Link to='/app/select/3'><img className="slideimg" src={vidcovlist[3]} alt="" /></Link>
            <Link to='/app/select/4'><img className="slideimg" src={vidcovlist[4]} alt="" /></Link>
          </Slider>
          <br/>
          <br/>
          <Slider {...settings}>
            <Link to='/app/select/5'><img className="slideimg" src={vidcovlist[5]} alt="" /></Link>
            <Link to='/app/select/6'><img className="slideimg" src={vidcovlist[6]} alt="" /></Link>
            <Link to='/app/select/7'><img className="slideimg" src={vidcovlist[7]} alt="" /></Link>
            <Link to='/app/select/8'><img className="slideimg" src={vidcovlist[8]} alt="" /></Link>
            <Link to='/app/select/9'><img className="slideimg" src={vidcovlist[9]} alt="" /></Link>
          </Slider>
        </Paper>
      </div>
    );
  }
}

class App extends Component {

  render() {

    return (
      <div>
        <Route path='/app' component={Nav}/>
        <Switch>
          <Route exact path='/app/select' component={Select}/>
          <Route path='/app/select/:videon' component={SpeechRecognition}/>
          <Route path='/app/stats' component={Profile}/>
          <Redirect to="/app/select" />
        </Switch>
      </div>
    );
  }
}


export default App;
