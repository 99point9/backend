"""p99 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from tastypie.api import Api
from profiles import views
from coursemanager.api import *
from profiles.api import *


course_api = Api(api_name='courses')
course_api.register(CourseResource())
course_api.register(LessonMaterialResource())
course_api.register(QuestionResource())
course_api.register(LessonPlanResource())
course_api.register(LessonResource())


profile_api = Api(api_name='profiles')
profile_api.register(UserResource())
profile_api.register(UserProfileResource())
profile_api.register(LoginRecordResource())
profile_api.register(UserLessonPlanResource())
profile_api.register(UserLessonResource())
profile_api.register(LessonRecordResource())
profile_api.register(SessionInformationResource())
profile_api.register(VideoPausesResource())
profile_api.register(QuestionDataResource())

urlpatterns = [
    url(r'^$', views.index),
    url(r'^api/', include(course_api.urls)),
    url(r'^api/', include(profile_api.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^nested_admin/', include('nested_admin.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
