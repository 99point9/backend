import React from 'react';
import { render } from 'react-dom';
import Login from './components/Login';
import App from './components/App';
import SpeechRecognition from './components/VideoPlayer';
import { BrowserRouter as Router,
          Route, Switch, Redirect, Link } from 'react-router-dom';
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
  red700, red900,
  deepOrangeA200,
  blueGrey100, blueGrey300, blueGrey400, blueGrey500,
  white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

const custmuiTheme = getMuiTheme({
  spacing: spacing,
  fontFamily: 'Gotham-Light, sans-serif',
  palette: {
    primary1Color: red700,
    primary2Color: red900,
    primary3Color: blueGrey400,
    accent1Color: deepOrangeA200,
    accent2Color: blueGrey100,
    accent3Color: blueGrey500,
    textColor: darkBlack,
    alternateTextColor: white,
    canvasColor: white,
    borderColor: blueGrey300,
    disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: red900,
    clockCircleColor: fade(darkBlack, 0.07),
    shadowColor: fullBlack,
  },
});

const Root = () => {
  return (
    <MuiThemeProvider muiTheme={custmuiTheme}>
      <Login />
    </MuiThemeProvider>
  )
}

render((
    <Router>
      <Root />
    </Router>
    ),
  document.getElementById('root')
)
