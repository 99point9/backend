import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import {Card, CardActions, CardHeader,
        CardMedia, CardTitle, CardText} from 'material-ui/Card';
import { BrowserRouter as Router,
          Route, Switch, Redirect, Link } from 'react-router-dom';
import FlatButton from 'material-ui/FlatButton';
import axios from 'axios';
import Slider from "react-slick";
import '../../styles/App.css';
import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
 margin: 20,
 width: 1280,
 height: 720,
};

class Profile extends Component {

  render() {

    return(
      <div>
        <Paper className='center-box' zDepth={2} style={style}>
          <h1>Stats Test</h1>
        </Paper>
      </div>
     )
  }

}

export default Profile;
