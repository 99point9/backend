import React, { Component } from 'react';
import '../../styles/Nav.css';
import axios from 'axios';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup,
      ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import { BrowserRouter as Router,
        Route, Switch, Redirect, Link } from 'react-router-dom';
import Snackbar from 'material-ui/Snackbar';

class Nav extends Component {

  constructor(props) {
    super(props);
    this.state = {
      autoHideDuration: 4000,
      message: 'Test Notification',
      open: false,
    };
  }

    handleClick = () => {
     this.setState({
       open: true,
     });
   };

   handleRequestClose = () => {
     this.setState({
       open: false,
     });
   };


  render() {
    return (
      <div>
        <Toolbar>
          <ToolbarGroup>
            <img className="logo"src="http://138.197.154.10/media/logo-red.png" alt="" />
            <ToolbarSeparator />
            <Link to='/app/select'><RaisedButton label="Courses" primary={true} /></Link>
          </ToolbarGroup>
          <ToolbarGroup lastChild={true}>
          <RaisedButton
                    onClick={this.handleClick}
                    label="Test Notification"
                  />
            <Link to='/app/stats'><RaisedButton label="Stats" primary={true} /></Link>

            <IconMenu
              iconButtonElement={
                <IconButton touch={true}>
                  <NavigationExpandMoreIcon />
                </IconButton>
              }
            >
              <MenuItem primaryText="Contact" />
              <MenuItem primaryText="About Us" />
              <MenuItem primaryText="Logout" />
            </IconMenu>
          </ToolbarGroup>
        </Toolbar>
        <Snackbar
            open={this.state.open}
            message={this.state.message}
            autoHideDuration={this.state.autoHideDuration}
            onRequestClose={this.handleRequestClose}
          />
      </div>
    );
  }
}

export default Nav;
